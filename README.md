# Graphql DEMO

## Learning

setup:

```bash
$ npm install
$ npm run dev
// browser to http://localhost:9001/graphql and play with GUI
```

1. Go check out the API
2. Go check out the tests folder
  - run the tests with `npm test`


## Notes

- merge requests most welcome!

## Todo

- add example mutation
