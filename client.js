const { ApolloClient } = require('@apollo/client/core')
const { InMemoryCache } = require('@apollo/client/cache')

const { HttpLink } = require('@apollo/client/link/http')
const fetch = require('cross-fetch')

module.exports = function graphqlClient (opts = {}) {
  const {
    port = 9001
  } = opts

  return new ApolloClient({
    link: new HttpLink({
      uri: `http://localhost:${port}/graphql`,
      fetch
    }),
    cache: new InMemoryCache(),
    ...opts
  })
}

/*
# NOTES

This graphql client is fairly customised, and a little non-standard.

## Hack 1

Problem: I wanted to use "require" instead of ES6 "import" so that I didn't have to bundle + transpile the code

Solution: figured out which files to grab exactly, and it worked?

If we used this in production you might miss out on some tree-shaking.

---

## Hack 2
Problem: @apollo/client has "react" a peer-dependency (complains if it's not installed)

Solution: Hack 1 fixed it

src: https://github.com/apollographql/apollo-client/issues/7318

---

## Hack 3
Problem: when this is run with Node, we don't have access to the browser "fetch" method for making API calls

Solution: provide a custom HttpLink which has a custom "fetch" method inserted.

This isn't needed in thw browser, you can replace the who link option with just uri

*/
