const server = require('./server')
const db = require('./test/mock-db')

const port = 9001
server({ db, port })

const msg = `Running GraphQL API at:
  http://localhost:${port}/graphql`

import('boxen')
  .then(boxen => {
    console.log(boxen.default(msg, {
      borderStyle: 'round',
      borderColor: 'magenta',
      padding: 1,
      margin: 1
    }))
  })
