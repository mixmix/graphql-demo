// from https://graphql.org/graphql-js/running-an-express-graphql-server/

const express = require('express')
const { graphqlHTTP } = require('express-graphql')
const { makeExecutableSchema } = require('@graphql-tools/schema')

const typeDefs = require('./type-defs')
const resolvers = require('./resolvers')

module.exports = function graphqlServer (opts = {}) {
  const {
    db,
    port = 9001
  } = opts

  const app = express()
  app.use('/graphql', graphqlHTTP({
    schema: makeExecutableSchema({
      typeDefs,
      resolvers: resolvers(db)
    }),
    rootValue: {},
    graphiql: true // a gui!
  }))

  const server = app.listen(port, console.log)

  return server
}
