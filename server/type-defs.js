module.exports = `
  type Query {
    serverTime: String

    books: [Book]
    book (id: Int!): Book
  }

  type Book {
    id: Int!
    title: String

    authorId: Int
    author: Author
  }

  type Author {
    id: Int!
    name: String!

    books: [Book]
  }
`
