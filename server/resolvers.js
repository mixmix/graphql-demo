module.exports = function (db) {
  return {
    Query: {
      serverTime: () => {
        return new Date().toString()
      },

      books (parent, args) {
        return db.getBooks()
      },

      book (parent, args) {
        return db.getBook(args.id)
      }
    },

    Book: {
      // id                get these by default
      // title
      // authorId
      author (parent) { // have to resolve this one
        const { authorId } = parent

        return db.getAuthor(authorId)
      }
    },

    Author: {
      // id
      // name
      books (parent) {
        const { id: authorId } = parent

        return db.getBooks({ authorId })
      }
    }
  }
}
