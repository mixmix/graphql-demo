const test = require('tape')
const gql = require('graphql-tag')

const Server = require('../server')
const Client = require('../client')

test('server startup', async t => {
  const server = Server()
  t.true(server, 'server started up')

  const client = Client()
  t.true(client, 'client started up')

  const res = await client.query({
    query: gql`{
      serverTime
    } `
  })
  t.match(res.data.serverTime, /Time/, 'query returns a time!')

  server.close()
  t.end()
})
