/* Author table */
const authors = [
  { id: 1, name: 'Ursula K. Le Guin' },
  { id: 12, name: 'Yanis Varoufakis' }
]

/* Book table */
const books = [
  { id: 1, title: 'Another Now', authorId: 12 },
  { id: 2, title: 'The Dispossessed', authorId: 1 },
  { id: 3, title: 'A Wizard of Earthsea', authorId: 1 }
]

const mockDB = {
  getBooks (opts = {}) {
    let result = books

    if (opts.authorId) result = result.filter(book => book.authorId === opts.authorId)

    return Promise.resolve(result)

    // same as
    // return new Promise((resolve, reject) => {
    //   resolve(result)
    // })
  },

  getBook (id) {
    const book = books.find(book => book.id === id)

    return Promise.resolve(book)
  },

  getAuthor (id) {
    const author = authors.find(author => author.id === id)

    return Promise.resolve(author)
  }
}

module.exports = mockDB
