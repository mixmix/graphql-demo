const test = require('tape')
const gql = require('graphql-tag')

const Server = require('../server')
const Client = require('../client')
const db = require('./mock-db')

test('book', async t => {
  const port = 3000 + Math.random() * 6000 | 0
  const server = Server({ db, port })
  const client = Client({ port })

  let res = await client.query({
    query: gql`{
      books {
        id
        title
        authorId
      }
    }`
  })

  t.deepLooseEqual(
    res.data.books,
    [
      { __typename: 'Book', id: 1, title: 'Another Now', authorId: 12 },
      { __typename: 'Book', id: 2, title: 'The Dispossessed', authorId: 1 },
      { __typename: 'Book', id: 3, title: 'A Wizard of Earthsea', authorId: 1 }
    ],
    'books query!'
  )

  res = await client.query({
    query: gql`{
      book (id: 2) {
        title
        author {
          name
        }
      }
    }`
  })

  t.deepLooseEqual(
    res.data.book,
    {
      __typename: 'Book',
      title: 'The Dispossessed',
      author: {
        __typename: 'Author',
        name: 'Ursula K. Le Guin'
      }
    },
    'book query!'
  )

  server.close()
  t.end()
})
